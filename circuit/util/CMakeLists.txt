cmake_minimum_required(VERSION 3.5)
project(util)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(SOURCES src/util.cpp)
set(HEADERS inc/util.hpp)

add_library(${PROJECT_NAME} SHARED ${SOURCES} ${HEADERS})

set_target_properties(${PROJECT_NAME} PROPERTIES PUBLIC_HEADER inc/util.hpp)

target_include_directories(${PROJECT_NAME} PUBLIC inc)

install(TARGETS ${PROJECT_NAME}
        LIBRARY DESTINATION lib/${PROJECT_NAME}
        PUBLIC_HEADER DESTINATION lib/${PROJECT_NAME})
